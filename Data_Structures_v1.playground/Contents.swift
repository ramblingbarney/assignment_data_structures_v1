import Cocoa

protocol NodeElement {
    var description: String { get }
}


public class Node: NodeElement, CustomStringConvertible {
    
    var next: Node?
    weak var previous: Node?
    var values: [String]
     
    init(value: [String]) {
        self.values = []
        value.map({ self.values.append($0) })
    }
    
    public var description: String {
        return values.joined(separator: " ")
    }
    
}


public class LinkedList: CustomStringConvertible {
    
    fileprivate var head: Node?
    private var tail: Node?

    public var isEmpty: Bool {
    return head == nil
    }

    public var first: Node? {
    return head
    }

    public var last: Node? {
    return tail
    }
    
    public var length: Int {
        var node = head
        var count = 0
        while node != nil {
            count += 1
          node = node!.next
        }
        return count
    }

    public func add(value: String...) -> Void {
        
        // 1
        let newNode = Node(value: value)
        // 2
        if let tailNode = tail {
          newNode.previous = tailNode
          tailNode.next = newNode
        }
        // 3
        else {
          head = newNode
        }
        // 4
        tail = newNode
    
    }
    
    public func nodeAt(index: Int) -> Node? {
      // 1
      if index >= 0 {
        var node = head
        var i = index
        // 2
        while node != nil {
          if i == 0 { return node }
          i -= 1
          node = node!.next
        }
      }
      // 3
      return nil
    }
    
    public func remove(node: Node) -> [String] {

      let prev = node.previous
      let next = node.next

      if let prev = prev {
        prev.next = next // 1
      } else {
        head = next // 2
      }
      next?.previous = prev // 3

      if next == nil {
        tail = prev // 4
      }

      // 5
      node.previous = nil
      node.next = nil

      // 6
      return node.values
    }
    
    public var description: String {
    
    var text = Array<String>()
      var node = head
      
      while node != nil {
        text.append("H-\(node!.values.joined(separator: "-"))-T")
        node = node!.next
      }
    
      return text.joined(separator: " ")
    }
    
}


class OrderedSet: LinkedList {
    
    var count: Int {
        get {
        
            let list = CreateOrderedSet()
        
            return list.count
        
            }
        
        
    }
    
    override var isEmpty: Bool {
        get {
            
            let list = CreateOrderedSet()
            
            if list.count == 0 {
                
                return false
            } else {
                
                return true
            }
        }
    }
    
    public func CreateOrderedSet() -> [String] {

        var elementList = Array<String>()
        var node = head
        
        if node == nil {
            return []
        }
        

        while node != nil {
            
            if elementList.contains( node!.values.joined(separator: " ") ) {
                
            } else {
                
                elementList.append(node!.values.joined(separator: " "))
                
            }
            
            node = node!.next
        }

        return elementList

    }

    
    public func contain(value: String) -> Bool {
        
        let list = CreateOrderedSet()
        
        if list.contains(value) {
            
            return true
        } else {
            return false
        }
        
    }
    
    public override var description: String {
        
        let list = CreateOrderedSet()
        
        let returnString = list.joined(separator: " ")
        
        return "[" + returnString + "]"

    }

}

let x = Node(value: ["Husky"])

let dogBreeds = OrderedSet()
dogBreeds.add(value: "Labrador Cat")
dogBreeds.add(value: "Bulldog")
dogBreeds.add(value: "Beagle")
dogBreeds.add(value: "Husky")
dogBreeds.add(value: "Husky")
dogBreeds.length
//dogBreeds.remove(node: x)
print(dogBreeds.description)
dogBreeds.CreateOrderedSet()
//dogBreeds.insert(value: "HuskyX")
dogBreeds.description
dogBreeds.count
dogBreeds.isEmpty
dogBreeds.contain(value: "")
